# What is my IP address

## 在线运行的地址(内含使用办法)

http://ip.nutz.cn

## 如何运行
这是一个maven工程, 使用[NutzBoot](https://gitee.com/nutz/nutzboot)作为底层, eclipse/idea均可按maven项目导入

MainLauncher是入口,启动即可

### 环境要求

* 必须JDK8+
* eclipse或idea等IDE开发工具,可选

### 命令下启动

仅供测试用,使用mvn命令即可

```
// for windows
set MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn clean compile nutzboot:run

// for *uix
export MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn clean compile nutzboot:run
```

## 相关资源
论坛: https://nutz.cn
官网: https://nutz.io
一键生成NB的项目: https://get.nutz.io