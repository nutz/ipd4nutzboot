FROM openjdk:15.0.2-jdk
#EXPOSE 8080
COPY target/ipd-1.0.1.jar /opt/app.jar 
WORKDIR /opt
CMD ["java", "-jar", "app.jar"]
